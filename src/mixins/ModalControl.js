export default {
    methods: {
        modalOpen(ref) {
            ref.open();
        },
        modalClose(ref) {
            ref.close();
        }
    }
}
